```
#!/bin/bash
sudo adduser --system --quiet --shell=/bin/bash --home=/opt/perpul --gecos 'perpul' --group perpul
sudo mkdir /etc/perpul && sudo mkdir /var/log/perpul/
sudo apt-get update && sudo apt-get upgrade -y && sudo apt-get install postgresql postgresql-server-dev-9.5 build-essential python3-pillow python3-lxml python-ldap3 python3-dev python3-pip python3-setuptools npm nodejs git gdebi libldap2-dev libsasl2-dev  libxml2-dev libxslt1-dev libjpeg-dev -y
git clone --depth=1 --branch=master https://gitlab.com/flectra-hq/flectra.git /opt/perpul/perpul
#sudo find /opt/perpul -type f -readable -writable -exec sed -i "s/flectra/perpul/g; s/FLECTRA/PERPUL/g; s/flectrahq.com/perpul.co/g; s/flectrahq/perpul/g; s/Flectra/Perpul/g; s/37ad7e/3930c1/g; s/DFA941/2d2698/g; s/1eabfe/7069F8/g; s/84cc33/433bc3/g; s/0016fb94/7169f9/g; s/c69817/4b316d/g; s/009efb/6800fa/g" {} \;
#sudo chown perpul:perpul /opt/perpul/ -R && sudo chown perpul:perpul /var/log/perpul/ -R && cd /opt/perpul/perpul && sudo pip3 install -r requirements.txt
sudo npm install -g less less-plugin-clean-css -y && sudo ln -s /usr/bin/nodejs /usr/bin/node
sudo apt install node-less node-clean-css -y && sudo ln -s /usr/bin/nodejs /usr/bin/node
cd /tmp && wget https://downloads.wkhtmltopdf.org/0.12/0.12.2.1/wkhtmltox-0.12.2.1_linux-trusty-amd64.deb && sudo gdebi -n wkhtmltox-0.12.2.1_linux-trusty-amd64.deb && rm wkhtmltox-0.12.2.1_linux-trusty-amd64.deb
sudo ln -s /usr/local/bin/wkhtmltopdf /usr/bin/ && sudo ln -s /usr/local/bin/wkhtmltoimage /usr/bin/
wget -N http://geolite.maxmind.com/download/geoip/database/GeoLiteCity.dat.gz && sudo gunzip GeoLiteCity.dat.gz && sudo mkdir /usr/share/GeoIP/ && sudo mv GeoLiteCity.dat /usr/share/GeoIP/
sudo service postgresql start
sudo su - postgres -c "createuser -s perpul"
sudo su - perpul -c "/opt/perpul/perpul/perpul-bin --addons-path=/opt/perpul/perpul/addons -s --stop-after-init"
sudo mv /opt/perpul/.perpulrc /etc/perpul/perpul.conf
sudo sed -i "s,^\(logfile = \).*,\1"/var/log/perpul/perpul-server.log"," /etc/perpul/perpul.conf
sudo sed -i "s,^\(logrotate = \).*,\1"True"," /etc/perpul/perpul.conf
sudo sed -i "s,^\(proxy_mode = \).*,\1"True"," /etc/perpul/perpul.conf
sudo cp /opt/perpul/perpul/debian/init /etc/init.d/perpul && chmod +x /etc/init.d/perpul
sudo ln -s /opt/perpul/perpul/perpul-bin /usr/bin/perpul
sudo update-rc.d -f perpul start 20 2 3 4 5 .
sudo service perpul start

```

